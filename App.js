import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput } from 'react-native';

export default function App() {
  const [gameStarted, setGameStarted] = useState(false);
  const [gameOver, setGameOver] = useState(false);
  const [score, setScore] = useState(0);
  const [timeRemaining, setTimeRemaining] = useState(15);
  const [ballPosition, setBallPosition] = useState({ x: 0, y: 0 });
  const [movementFrequency, setMovementFrequency] = useState(1000);
  const [name, setName] = useState('');

  const gameTimer = useRef(null);

  useEffect(() => {
    if (gameStarted && !gameOver) {
      gameTimer.current = setInterval(() => {
        setTimeRemaining((prevTime) => {
          if (prevTime === 0) {
            endGame();
            return prevTime;
          }
          return prevTime - 1;
        });
        setBallPosition(generateRandomPosition());
      }, movementFrequency);
    } else {
      clearInterval(gameTimer.current);
    }

    return () => clearInterval(gameTimer.current);
  }, [gameStarted, gameOver, movementFrequency]);

  //Função startGame: Responsável por iniciar o jogo, reiniciando todas as variáveis do estado para os valores iniciais.
  const startGame = () => {
    setGameStarted(true);
    setGameOver(false);
    setScore(0);
    setTimeRemaining(15);
    setBallPosition(generateRandomPosition());
    setMovementFrequency(1000);
  };

  //Função endGame: É chamada quando o tempo restante chega a zero. Ela encerra o jogo.
  const endGame = () => {
    clearInterval(gameTimer.current);
    setGameStarted(false);
    setGameOver(true);
  };

  //Função handleBallPress: É chamada quando o jogador clica na bola durante o jogo. Ela realiza ações relacionadas ao jogo em andamento.
  const handleBallPress = () => {
    if (gameStarted && !gameOver) {
      setScore((prevScore) => prevScore + 1);
      setBallPosition(generateRandomPosition());
      setMovementFrequency((prevFrequency) => prevFrequency * 0.9);
    }
  };

  //Função generateRandomPosition: Essa função gera uma nova posição aleatória para a bola dentro dos limites da tela.
  const generateRandomPosition = () => {
    const maxX = window.innerWidth - 50;
    const maxY = window.innerHeight - 50;
    const randomX = Math.floor(Math.random() * maxX);
    const randomY = Math.floor(Math.random() * maxY);
    return { x: randomX, y: randomY };
  };

  return (
    <View style={styles.container}>
      {!gameStarted && !gameOver && (
        <View style={styles.welcomeContainer}>
          <Text style={styles.welcomeText}>Bem-vindo!</Text>
          <TextInput
            style={styles.nameInput}
            placeholder="Digite seu nome"
            value={name}
            onChangeText={setName}
          />
          <TouchableOpacity style={styles.startButton} onPress={startGame}>
            <Text style={styles.buttonText}>Iniciar</Text>
          </TouchableOpacity>
        </View>
      )}

      {(gameStarted || gameOver) && (
        <View style={styles.scoreContainer}>
          <Text style={styles.scoreText}>Pontuação: {score}</Text>
          <Text style={styles.timeText}>Tempo restante: {timeRemaining}</Text>
        </View>
      )}

      {gameStarted && !gameOver && (
        <TouchableOpacity
          style={[styles.ball, { top: ballPosition.y, left: ballPosition.x }]}
          onPress={handleBallPress}
        />
      )}

      {gameOver && (
        <View style={styles.endGameContainer}>
          <Text style={styles.endGameText}>Fim de Jogo!</Text>
          <Text style={styles.endGameScore}>Pontuação: {score}</Text>
          <TouchableOpacity style={styles.restartButton} onPress={startGame}>
            <Text style={styles.buttonText}>Recomeçar</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.exitButton} onPress={() => setGameOver(false)}>
            <Text style={styles.buttonText}>Encerrar</Text>
          </TouchableOpacity>
        </View>
      )}

      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  welcomeContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  welcomeText: {
    fontSize: 24,
    marginBottom: 20,
  },
  startButton: {
    backgroundColor: '#4CAF50',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
  },
  scoreContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: 50,
    backgroundColor: '#f5f5f5',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  scoreText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  timeText: {
    fontSize: 16,
  },
  ball: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: 'red',
    position: 'absolute',
  },
  endGameContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  endGameText: {
    fontSize: 24,
    marginBottom: 20,
  },
  endGameScore: {
    fontSize: 18,
    marginBottom: 20,
  },
  restartButton: {
    backgroundColor: '#4CAF50',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
    marginBottom: 10,
  },
  exitButton: {
    backgroundColor: '#f44336',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5,
  },
  nameInput: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    padding: 10,
    width: 200,
    marginBottom: 20,
  },
});